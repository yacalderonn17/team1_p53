CICLO 3 MINTIC - UNAL
este repositorio contiene el código implementado para dar cumplimiento al ciclo de 3 del programa misión TIC 2022,
el proyecto asignado trata de dar solución mediante una herramienta tecnólogica a un Banco

para instalar todas las librerias:
    pip install -r requirements.txt

Se modela el protecto con las siguientes tablas (modelos):
    - user: manejo de usuarios
    - products: cuentas de ahorro o tarjetas de crédito
    - account: cuentas de ahorro
    - creditCard: tarjetas de créedito
    - movement: movimientos


Para crear usuario
{
    "username": "alexis",
    "password": "123123123",
    "name": "alexis",
    "email": "correo@electronico.com",
    "birthday": "1996-03-17",
    "last_name": "Calderon",
    "address": "cra16 a 123",
    "identification": "1234586565",
    "phone": "3101231234",
    "is_superuser":true
}

{
    "user":1,
    "type_product": "creditCard",
    "info_product": {
        "creditLimit": 2000000
    }
}

{
    "user":2,
    "type_product": "account",
    "info_product": {}
}