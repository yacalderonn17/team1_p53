
from django.db import models
from .product import Product
import datetime

class Account(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, related_name='account', on_delete=models.CASCADE)
    amount = models.BigIntegerField('Amount')
    number = models.CharField('Number',max_length=19, unique=True) # (xxxx xxxx xxxx xxxx)
    lastChangeDate = models.DateTimeField()


    def add(self, value):
        self.lastChangeDate = datetime.datetime.now()
        self.amount = self.amount + value
        super().save()

    def subtract(self,value):
        self.lastChangeDate = datetime.datetime.now()
        self.amount = self.amount - value
        super().save()
    
    def available(self,value):
        return self.amount - value >= 0