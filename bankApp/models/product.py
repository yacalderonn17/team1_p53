from django.db import models
from django.db.models import Manager, Model
from .user import User
import json


class ProductManager(Manager):
    def create_product(self, **kwargs):
        product = self.create(**kwargs)
        return product

class Product(Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    isActive = models.BooleanField(default=True)

    def save(self, **kwargs):
        super().save(**kwargs)

    # solo se permitira actualizar si el prducto está activo o no
    def update(self,data):
        if 'isActive' in data:
            self.isActive = data['isActive']
        super().save()
    
    
    def delete(self):
        super().delete()

    objects = ProductManager()



    