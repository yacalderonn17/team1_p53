from django.db import models
from .product import Product

class CreditCard(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, related_name='creditCard', on_delete=models.CASCADE)
    number = models.CharField('Number',max_length=19, unique=True) # (xxxx xxxx xxxx xxxx)
    cvc = models.CharField('cvc',  max_length=3)
    expiredDate = models.DateTimeField()
    cutDate = models.DateTimeField()
    debt = models.BigIntegerField('Debt')
    creditLimit = models.BigIntegerField('CreditLimit')
    
    def available(self,value):
        return (self.creditLimit - debt - value) >= 0
    
    def addToDebt(self, value):
        self.debt = self.debt + value
        super().save()

    def subtractToDebt(self,value):
        self.debt = self.debt - value
        super().save()
    