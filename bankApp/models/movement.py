from django.db import models
from .product import Product

class Movement(models.Model):
    id = models.AutoField(primary_key=True)
    MovementDate = models.DateTimeField()
    origin = models.ForeignKey(Product, related_name='origin', on_delete=models.CASCADE)
    destiny = models.ForeignKey(Product, related_name='destiny', on_delete=models.CASCADE)
    successful = models.BooleanField(default=True)
    details = models.CharField('Details', max_length = 256)
