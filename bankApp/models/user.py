# la base de este archivo sirve para nuestro poryecto, faltaria agregar todas las columnas al modelo que hagan falta
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

# clase para manejar usuarios ()
class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(
        username=username,
        password=password,
        )
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField('Username', max_length = 15, unique=True)
    password = models.CharField('Password', max_length = 256)
    name = models.CharField('Name', max_length = 30)
    last_name = models.CharField('Last_name', max_length = 30)
    identification = models.CharField('Identification', max_length = 15)
    email = models.EmailField('Email', max_length = 100)
    address = models.CharField('Address', max_length = 100)
    phone = models.CharField('Phone', max_length = 10)
    birthday = models.DateField('Birthday')
    
    
    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)


    def update(self, data):
        # se filtra cuales atributos se pueden actualizar
        if 'email' in data:
            self.email = data['email']
        
        if 'address' in data:
            self.address = data['address']

        if 'phone' in data:
            self.phone = data['phone']        
        
        super().save()

    def delete(self):
        super().delete()


    objects = UserManager()
    USERNAME_FIELD = 'username'