from bankApp.models.product import Product
from bankApp.models.user import User
from bankApp.models.account import Account
from bankApp.models.creditCard import CreditCard


from rest_framework import serializers


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id','user','isActive']

    def create(self, validated_data):
        productInstance = Product.objects.create_product(**validated_data)
        return productInstance


    def to_representation(self, obj):
        product = Product.objects.get(id=obj.id)
        credit = product.creditCard.all()
        account = product.account.all()
        if len(credit) > 0:
            detailsProduct = {
                'number': credit[0].number,
                'debt': credit[0].debt,
                'CreditLimit': credit[0].creditLimit,
            }
        elif len(account) > 0:
            detailsProduct = {
                'number': account[0].number,
                'amount': account[0].amount,
                'lastChangeDate': account[0].lastChangeDate
            }
        return {
            'id': product.id,
            'user': product.user.id,
            'username': product.user.username,
            'isActive': product.isActive,
            'product': detailsProduct
        }