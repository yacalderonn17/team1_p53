from bankApp.models.movement import Movement
from rest_framework import serializers
class MovementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movement
        fields = ['id', 'MovementDate', 'origin', 'destiny', 'successful', 'details']

        