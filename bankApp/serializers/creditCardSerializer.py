from bankApp.models.creditCard import CreditCard
from rest_framework import serializers
class CeditCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = CreditCard
        fields = ['id', 'product', 'number', 'cvc', 'expiredDate', 'cutDate', 'debt', 'creditLimit']