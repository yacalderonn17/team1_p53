from rest_framework import serializers
from bankApp.models.user import User
from bankApp.models.account import Account
from bankApp.serializers.accountSerializer import AccountSerializer

class UserSerializer(serializers.ModelSerializer):
    #account = AccountSerializer()
    class Meta:
        model = User
        fields = ['id','username','password','name','last_name','identification','email','address','phone','birthday','is_superuser']

    def create(self, validated_data):
        #accountData = validated_data.pop('account')
        userInstance = User.objects.create(**validated_data)
        #Account.objects.create(user=userInstance, **accountData)
        return userInstance

    #if you don't wanna share all information, you should change to_representation
    
    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        #account = Account.objects.get(user=obj.id)
        products = []
        
        
        prods = user.product_set.all()
        if len(prods) > 0:
            for p in prods:

                credit = p.creditCard.all()
                account = p.account.all()

                if len(credit) > 0:
                    detailsProduct = {
                        'id': p.id,
                        'isActive': p.isActive,
                        'number': credit[0].number,
                        'debt': credit[0].debt,
                        'CreditLimit': credit[0].creditLimit,
                    }
                    
                elif len(account) > 0:
                    detailsProduct = {
                        'id': p.id,
                        'isActive': p.isActive,
                        'number': account[0].number,
                        'lastChangeDate': account[0].lastChangeDate,
                    }
                products.append(detailsProduct)

        return {
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'last_main': user.last_name,
            'identification': user.identification,
            'email': user.email,
            'phone': user.phone,
            'is_superuser': user.is_superuser,
            'products': products
        }