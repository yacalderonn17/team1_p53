from django.contrib import admin
from .models.user import User
from .models.account import Account
from .models.creditCard import CreditCard
from .models.product import Product
from .models.movement import Movement

# Registrar todos los modelos
admin.site.register(Product)
admin.site.register(User)
admin.site.register(Account)
admin.site.register(CreditCard)
admin.site.register(Movement)