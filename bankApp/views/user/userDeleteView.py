from django.conf import settings
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from bankApp.serializers.userSerializer import UserSerializer
from rest_framework.permissions import IsAuthenticated
from bankApp.models.user import User

class UserDeleteView(generics.RetrieveAPIView):
    
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        
        if valid_data['user_id'] != kwargs['pk'] and not(User.objects.get(id=valid_data['user_id']).is_superuser) :
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        
        serializer_class = UserSerializer
        user = User.objects.get(id=kwargs['pk'])
        user.delete()
        
        
        return Response({'ok':'true'}, status=status.HTTP_200_OK)