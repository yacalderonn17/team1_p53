from .user.userCreateView import UserCreateView
from .user.userDetailView import UserDetailView
from .user.userUpdateView import UserUpdateView
from .user.userDeleteView import UserDeleteView

from .product.productCreateView import ProductCreateView
from .product.productUpdateView import ProductUpdateView
from .product.productDetailView import ProductDetailView

from .movement.consignmentView import ConsignmentView
from .movement.movementView import MovementView
