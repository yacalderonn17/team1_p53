from django.conf import settings
from rest_framework import status, views, generics
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from bankApp.serializers.productSerializer import ProductSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from bankApp.models.user import User
from bankApp.models.product import Product



class ProductDetailView(generics.RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        
        print(valid_data['user_id'])
        print(Product.objects.get(id=kwargs['pk']).user.id)
        # restringe la vista a quienes no son superusuarios y no les pertenece la cuenta
        if valid_data['user_id'] != Product.objects.get(id=kwargs['pk']).user.id and not(User.objects.get(id=valid_data['user_id']).is_superuser): 
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
  
       
        return super().get(request, *args, **kwargs)
        