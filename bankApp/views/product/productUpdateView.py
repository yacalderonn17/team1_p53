from django.conf import settings
from rest_framework import status, views, generics
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from bankApp.serializers.productSerializer import ProductSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from bankApp.models.user import User
from bankApp.models.product import Product


class ProductUpdateView(generics.RetrieveAPIView):
   
    def patch(self, request, *args, **kwargs):
        permission_classes = (IsAuthenticated,)

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        
        # el estado solo lo puede modificar un superusuario
        if not(User.objects.get(id=valid_data['user_id']).is_superuser): 
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
  

        serializer_class = ProductSerializer
        product = Product.objects.get(id=kwargs['pk'])
        product.update(request.data)
        
        return Response({'ok':'true'},status=status.HTTP_200_OK)
        