from django.conf import settings
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from bankApp.serializers.productSerializer import ProductSerializer
from bankApp.serializers.creditCardSerializer import CeditCardSerializer
from bankApp.serializers.accountSerializer import AccountSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from bankApp.models.user import User
from random import randint
import datetime


class ProductCreateView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        
        # crear productos solo deberia poder hacerlo el superusuario
        if not(User.objects.get(id=valid_data['user_id']).is_superuser): 
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
  
        serializer = ProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        newProduct = serializer.save()
        if newProduct:
            type_product = request.data['type_product']
            info_product = request.data['info_product']
            if type_product == "account":
                data = {
                    'product': newProduct.id,
                    'number': str(randint(1000, 9999)) + " " + str(randint(1000, 9999)) + " " + str(randint(1000, 9999)) + " " + str(randint(1000, 9999)),
                    'amount': 0,
                    'lastChangeDate': datetime.datetime.now()
                }
                
                serializer_pr = AccountSerializer(data=data)
                serializer_pr.is_valid(raise_exception=True)
                serializer_pr.save()

            elif type_product == "creditCard":
                data = {
                    'product': newProduct.id,
                    'number': str(randint(1000, 9999)) + " " + str(randint(1000, 9999)) + " " + str(randint(1000, 9999)) + " " + str(randint(1000, 9999)),
                    'cvc': str(randint(100, 999)),
                    'expiredDate': datetime.datetime.now() + datetime.timedelta(days=1000),
                    'cutDate': datetime.datetime.now() + datetime.timedelta(days=30),
                    'debt': 0,
                    'creditLimit': info_product["creditLimit"]
                }
                serializer_pr = CeditCardSerializer(data=data)
                serializer_pr.is_valid(raise_exception=True)
                serializer_pr.save()

        return Response({'id':newProduct.id}, status=status.HTTP_201_CREATED)