from django.conf import settings
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from bankApp.serializers.movementSerializer import MovementSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from bankApp.models.user import User
from bankApp.models.product import Product
from bankApp.models.account import Account
import datetime


class MovementView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        
        origen = Product.objects.get(id=request.data['origin'])
        # el movimiento solo la puede hacer la persona dueña del producto de origen 
        if not(User.objects.get(id=valid_data['user_id']) == origen.user): 
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        fechayhora = datetime.datetime.now() 
        data = {
            'MovementDate': fechayhora,
            'origin': request.data['origin'], 
            'destiny': request.data['destiny'] 
        }

        if origen.isActive:
            account = origen.account.all()
            credit_card = origen.creditCard.all()
            producto_origen = None

            if len(account) > 0:
                producto_origen = account[0]
            elif len(credit_card) > 0:
                producto_origen = credit_card[0]

            if producto_origen.available(request.data['value']):
                destino = Product.objects.get(id=request.data['destiny'])
                if destino.isActive:
                    account_dest = destino.account.all()
                    credit_card_dest = destino.creditCard.all()

                    # salida al producto de origen
                    if len(account) > 0:
                        producto_origen.subtract(request.data['value'])
                    elif len(credit_card) > 0:
                        producto_origen.addToDebt(request.data['value'])
                        
                
                    # entrada al producto de destino
                    if len(account_dest) > 0:
                        account_dest[0].add(request.data['value'])
                    elif len(credit_card_dest) > 0:
                        credit_card_dest[0].subtractToDebt(request.data['value'])

                    data['successful'] = True
                    data['details'] = "Movimiento exitoso - valor: " + str(request.data['value'])
                else:
                    data['successful'] = False
                    data['details'] = "Producto de destino Inactivo"


            else:
                data['successful'] = False
                data['details'] = "Fondos insuficientes"
            
                
        else:
            data['successful'] = False
            data['details'] = "Cuenta de Origen Inactivo"
            
        serializer = MovementSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        newMovement = serializer.save()

        return Response(data, status=status.HTTP_201_CREATED)


        