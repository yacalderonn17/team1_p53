from django.conf import settings
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from bankApp.serializers.movementSerializer import MovementSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from bankApp.models.user import User
from bankApp.models.product import Product
from bankApp.models.account import Account
import datetime


class ConsignmentView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        
        # la consignación solo deberia poder hacerla el superusuario
        if not(User.objects.get(id=valid_data['user_id']).is_superuser): 
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        fechayhora = datetime.datetime.now()
    
        product = Product.objects.get(id=request.data['product'])
        if product.isActive:
            cuenta = product.account.all()
            if len(cuenta) > 0:
                cuenta = cuenta[0]
                cuenta.add(request.data['value'])
            
                data = {
                    'MovementDate': fechayhora,
                    'origin': request.data['product'], 
                    'destiny': request.data['product'],
                    'successful': True, 
                    'details': "Consignación exitosa: " + str(request.data['value']) 
                }
            
                serializer = MovementSerializer(data=data)
                serializer.is_valid(raise_exception=True)
                newMovement = serializer.save()
        else:
            data = {
                'status': "producto desactivado"
            }
            
        
        return Response(data, status=status.HTTP_201_CREATED)