"""bank URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from bankApp import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    
    #usuarios - : Leer, Crear, Actualizar y Eliminar.
    path('user/create/', views.UserCreateView.as_view()), #crear
    path('user/<int:pk>/', views.UserDetailView.as_view()), #leer
    path('user/<int:pk>/update/', views.UserUpdateView.as_view()), # actualizar
    path('user/<int:pk>/delete/', views.UserDeleteView.as_view()), #eliminar
    
    # productos
    path('product/create/', views.ProductCreateView.as_view()),
    path('product/<int:pk>/', views.ProductDetailView.as_view()),
    path('product/<int:pk>/updateIsActive/',views.ProductUpdateView.as_view()),

    # movement
    path('movement/consignment/', views.ConsignmentView.as_view()),
    path('movement/', views.MovementView().as_view()),



]
